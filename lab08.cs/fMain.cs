﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab08
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnTablet_Click(object sender, EventArgs e)
        {
           Tablet tablet = new Tablet();
           fTablet fb = new fTablet(tablet);
           if (fb.ShowDialog() == DialogResult.OK)
           {
               tbTabletsInfo.Text += string.Format("Назва планшету: {0}.\r\nВиробник: {1}. \r\nКолір: {2}.\r\nКількість пам'яті: {3}.\r\nКількість планшетів: {4} планшетів.\r\nЦіна: {5:0.00} грн. \r\nКількість користувачів: {6}.\r\nМожливий дохід: {7:0.00} грн. \r\n \r\n", tablet.Name, tablet.Manufacturer, tablet.Colour, tablet.Memory, tablet.Amount, tablet.Cost, tablet.Users, tablet.GetYearIncomePerInhabitant());
           }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?","Припинити роботу", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit(); 
        }
    }
}
