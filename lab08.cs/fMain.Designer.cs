﻿namespace lab08
{
    partial class fMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTabletsInfo = new System.Windows.Forms.TextBox();
            this.btnTablet = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbTabletsInfo
            // 
            this.tbTabletsInfo.Location = new System.Drawing.Point(12, 12);
            this.tbTabletsInfo.Multiline = true;
            this.tbTabletsInfo.Name = "tbTabletsInfo";
            this.tbTabletsInfo.ReadOnly = true;
            this.tbTabletsInfo.Size = new System.Drawing.Size(538, 427);
            this.tbTabletsInfo.TabIndex = 0;
            // 
            // btnTablet
            // 
            this.btnTablet.Location = new System.Drawing.Point(575, 12);
            this.btnTablet.Name = "btnTablet";
            this.btnTablet.Size = new System.Drawing.Size(94, 35);
            this.btnTablet.TabIndex = 1;
            this.btnTablet.Text = "Додати планшет";
            this.btnTablet.UseVisualStyleBackColor = true;
            this.btnTablet.Click += new System.EventHandler(this.btnTablet_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(575, 416);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(89, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Вихід";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 451);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnTablet);
            this.Controls.Add(this.tbTabletsInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №8";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTabletsInfo;
        private System.Windows.Forms.Button btnTablet;
        private System.Windows.Forms.Button btnExit;
    }
}

