﻿namespace lab13
{
    partial class fTablet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbTablet = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.ComboBox();
            this.tbUsers = new System.Windows.Forms.TextBox();
            this.tbCost = new System.Windows.Forms.TextBox();
            this.tbAmount = new System.Windows.Forms.TextBox();
            this.tbMemory = new System.Windows.Forms.TextBox();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.tbColour = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbTablet.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbTablet
            // 
            this.gbTablet.Controls.Add(this.tbName);
            this.gbTablet.Controls.Add(this.tbUsers);
            this.gbTablet.Controls.Add(this.tbCost);
            this.gbTablet.Controls.Add(this.tbAmount);
            this.gbTablet.Controls.Add(this.tbMemory);
            this.gbTablet.Controls.Add(this.tbManufacturer);
            this.gbTablet.Controls.Add(this.tbColour);
            this.gbTablet.Controls.Add(this.label7);
            this.gbTablet.Controls.Add(this.label6);
            this.gbTablet.Controls.Add(this.label5);
            this.gbTablet.Controls.Add(this.label3);
            this.gbTablet.Controls.Add(this.label4);
            this.gbTablet.Controls.Add(this.label2);
            this.gbTablet.Controls.Add(this.label1);
            this.gbTablet.Location = new System.Drawing.Point(28, 24);
            this.gbTablet.Name = "gbTablet";
            this.gbTablet.Size = new System.Drawing.Size(342, 335);
            this.gbTablet.TabIndex = 0;
            this.gbTablet.TabStop = false;
            this.gbTablet.Text = "Загальні дані";
            // 
            // tbName
            // 
            this.tbName.FormattingEnabled = true;
            this.tbName.Items.AddRange(new object[] {
           "Apple",
            "Acer",
            "Asus",
            "Lenovo",
            "Huawei",
            "Samsung",
            "Pixus",
            "Panasonic",
            "Mystery",
            "Prestigio",
            "Nomi",
            "Sigma",
            "HP",
            "Xiaomi",
            "Microsoft",
            "Dell",
            "Sony",
            "HTC",
            "Fly",
            "Google",
            "Onda",
            "Sonyxperia",
            "LenovoPlaza",
            "RoverPad",
            "Explay",
            "Xoro",
            "Ainol",
            "EXEQ",
            "DFUNC"});
            this.tbName.Location = new System.Drawing.Point(175, 32);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 21);
            this.tbName.TabIndex = 14;
            // 
            // tbUsers
            // 
            this.tbUsers.Location = new System.Drawing.Point(175, 267);
            this.tbUsers.Name = "tbUsers";
            this.tbUsers.Size = new System.Drawing.Size(100, 20);
            this.tbUsers.TabIndex = 13;
            // 
            // tbCost
            // 
            this.tbCost.Location = new System.Drawing.Point(175, 231);
            this.tbCost.Name = "tbCost";
            this.tbCost.Size = new System.Drawing.Size(100, 20);
            this.tbCost.TabIndex = 12;
            // 
            // tbAmount
            // 
            this.tbAmount.Location = new System.Drawing.Point(175, 193);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.Size = new System.Drawing.Size(100, 20);
            this.tbAmount.TabIndex = 11;
            // 
            // tbMemory
            // 
            this.tbMemory.Location = new System.Drawing.Point(175, 155);
            this.tbMemory.Name = "tbMemory";
            this.tbMemory.Size = new System.Drawing.Size(100, 20);
            this.tbMemory.TabIndex = 10;
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Location = new System.Drawing.Point(175, 70);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(100, 20);
            this.tbManufacturer.TabIndex = 8;
            // 
            // tbColour
            // 
            this.tbColour.Location = new System.Drawing.Point(175, 111);
            this.tbColour.Name = "tbColour";
            this.tbColour.Size = new System.Drawing.Size(100, 20);
            this.tbColour.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Кількість користувачів";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ціна";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Кількість планшетів";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Колір";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Кількість пам\'ять";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Виробник";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Назва планшету";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(396, 49);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(396, 87);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Скасувати";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // fTablet
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(498, 386);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbTablet);
            this.MaximizeBox = false;
            this.Name = "fTablet";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Дані про новий планшет";
            this.Load += new System.EventHandler(this.fTablet_Load);
            this.gbTablet.ResumeLayout(false);
            this.gbTablet.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTablet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbUsers;
        private System.Windows.Forms.TextBox tbCost;
        private System.Windows.Forms.TextBox tbAmount;
        private System.Windows.Forms.TextBox tbMemory;
        private System.Windows.Forms.TextBox tbManufacturer;
        private System.Windows.Forms.TextBox tbColour;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox tbName;
    }
}