﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace lab14
{
    class Program
    {
        static List<Tablet> tablets;
        static void Main(string[] args)
        {
        tablets = new List<Tablet>();
        FileStream fs = new FileStream("Tablets.tablets", FileMode.Open);
        BinaryReader reader = new BinaryReader(fs);
            try
            {
                Tablet tablet;
                Console.WriteLine(" Читаємо данi з файлу...\n");
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    tablet = new Tablet();
                    for (int i = 1; i <= 8; i++)
                    {
                        switch (i)
                        {
                            case 1:
                                tablet.Name = reader.ReadString();
                                break;
                            case 2:
                                tablet.Manufacturer = reader.ReadString();
                                break;
                            case 3:
                                tablet.Colour = reader.ReadString();
                                break;
                            case 4:
                                tablet.Memory = reader.ReadInt32();
                                break;
                            case 5:
                                tablet.Amount = reader.ReadInt32();
                                break;
                            case 6:
                                tablet.Cost = reader.ReadDouble();
                                break;
                            case 7:
                                tablet.Users = reader.ReadInt32();
                                break;
                        }
                    }
                    tablets.Add(tablet);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Сталась помилка: {0}", ex.Message);
            }
            finally
            {
                reader.Close();
            }
            Console.WriteLine(" Несортований перелiк планшетів: {0}",
            tablets.Count); PrintTablets(); tablets.Sort();
            Console.WriteLine(" Сортований перелiк планшетів: {0}", tablets.Count);
            PrintTablets();
            Console.WriteLine(" Додаємо новий запис: Asus ");
            Tablet companyAsus = new Tablet("Asys", "TH Tung", "Company", 997000, 4000000, 237, 3245123);
            tablets.Add(companyAsus);
            tablets.Sort();
            Console.WriteLine(" Перелiк планшетів: {0}", tablets.Count);
            PrintTablets();
            Console.WriteLine(" Видаляємо останнє значення");
            tablets.RemoveAt(tablets.Count - 1);
            Console.WriteLine(" Перелiк планшетів: {0}", tablets.Count);
            PrintTablets();
            Console.ReadKey();

        }
        static void PrintTablets()
        {
            foreach (Tablet tablet in tablets)
            {
                Console.WriteLine(tablet.Info().Replace('і', 'i'));
            }
            Console.WriteLine();
        }
    }
}
